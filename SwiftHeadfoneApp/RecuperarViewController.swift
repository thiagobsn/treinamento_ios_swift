//
//  RecuperarViewController.swift
//  SwiftHeadfoneApp
//
//  Created by aluno-r17 on 09/07/15.
//  Copyright (c) 2015 TBSN. All rights reserved.
//

import UIKit

class RecuperarViewController: UIViewController {

    @IBOutlet weak var tfEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 61.0/255.0, green: 88.0/255.0, blue: 148.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func recuperarSenha(sender: AnyObject) {
        
        var email = self.tfEmail.text
        
        if !isValidEmail(email) {
            var alert = UIAlertView()
            alert.title = "ERRO!"
            alert.message = "Informe um email valido!"
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        
        recuperarSenhaPorEmail()
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    func recuperarSenhaPorEmail(){
        var manager = AFHTTPRequestOperationManager()
        var params = [ "email":self.tfEmail.text.lowercaseString]
        
        manager.POST("http://45.55.74.184:8080/sign/request_reset_password/", parameters: params, success: { (operation:AFHTTPRequestOperation!, responseObject: AnyObject) -> Void in
                var serviceMessage: String? = operation.responseString
                let alert = UIAlertView()
                alert.message = serviceMessage
                alert.addButtonWithTitle("OK")
                alert.show()
            
                self.navigationController?.popToRootViewControllerAnimated(true)
            
            }) { (operation:AFHTTPRequestOperation!, erro:NSError) -> Void in
            
                var erroMessage: String? = operation.responseString
                let alert = UIAlertView()
                alert.title = "Erro!"
                alert.message = erroMessage
                alert.addButtonWithTitle("OK")
                alert.show()
                
        }

        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
