//
//  CadastroViewController.swift
//  SwiftHeadfoneApp
//
//  Created by aluno-r17 on 09/07/15.
//  Copyright (c) 2015 TBSN. All rights reserved.
//

import UIKit

class CadastroViewController: UIViewController {


    @IBOutlet weak var tfNome: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfSenha: UITextField!
    
    var loading:UIAlertView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 61.0/255.0, green: 88.0/255.0, blue: 148.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cadastrar(sender: AnyObject) {
        
        var alertErro:UIAlertView!
        
        var nome = tfNome.text
        var email = tfEmail.text
        var senha = tfSenha.text
        
        if count(nome) == 0{
            alertErro = UIAlertView()
            alertErro.title = "ERRO!"
            alertErro.message = "Informe seu nome!"
            alertErro.addButtonWithTitle("OK")
            alertErro.show()
            return
        }
        
        if !isValidEmail(email) {
            alertErro = UIAlertView()
            alertErro.title = "ERRO!"
            alertErro.message = "Informe um email valido!"
            alertErro.addButtonWithTitle("OK")
            alertErro.show()
            return
        }
        
        if  count(senha) < 6 {
            alertErro = UIAlertView()
            alertErro.title = "ERRO!"
            alertErro.message = "Digite uma senha valida!"
            alertErro.addButtonWithTitle("OK")
            alertErro.show()
            return
        }
        
        
        self.loading = UIAlertView()
        self.loading.message = "Relizando cadastro"
        self.loading.show()
        
        singUpUser()
        
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    func singUpUser(){
        var manager = AFHTTPRequestOperationManager()
        
        var params = ["username":self.tfNome.text.lowercaseString, "email":self.tfEmail.text.lowercaseString,"password":self.tfSenha.text.lowercaseString]
        
        
        manager.POST("http://45.55.74.184:8080/users/sign_up/", parameters: params, success: { (operation:AFHTTPRequestOperation!, responseObject: AnyObject) -> Void in
            
                self.loading.dismissWithClickedButtonIndex(0, animated: true)
            
                let alert = UIAlertView()
                alert.message = "Cadastro realizar com sucesso!"
                alert.addButtonWithTitle("OK")
                alert.show()
            
                var token = responseObject.objectForKey("token") as! String
                var defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(token, forKey:"token")
            
                //self.dismissViewControllerAnimated(true, completion: nil)
                self.navigationController?.popToRootViewControllerAnimated(true)
            
            }) { (operation:AFHTTPRequestOperation!, erro:NSError) -> Void in
            
                var erroMessage: String? = operation.responseString
                
                let alert = UIAlertView()
                alert.title = "Erro!"
                alert.message = erroMessage
                alert.addButtonWithTitle("OK")
                alert.show()
                
                var defaults = NSUserDefaults.standardUserDefaults()
                defaults.removeObjectForKey("token")
                self.loading.dismissWithClickedButtonIndex(0, animated: true)
                
        }

    }


    @IBAction func lowerCaseNome(sender: AnyObject) {
        self.tfNome.text = self.tfNome.text.lowercaseString
    }
    
    @IBAction func lowercaseEmail(sender: AnyObject) {
        self.tfEmail.text = self.tfEmail.text.lowercaseString
    }
    
    @IBAction func lowercaseSenha(sender: AnyObject) {
        self.tfSenha.text = self.tfSenha.text.lowercaseString
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
