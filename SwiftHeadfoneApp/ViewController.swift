//
//  ViewController.swift
//  SwiftHeadfoneApp
//
//  Created by aluno-r17 on 08/07/15.
//  Copyright (c) 2015 TBSN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfSenha: UITextField!
    
    var loading:UIAlertView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }

    @IBAction func login(sender: AnyObject) {
        
        var email = tfEmail.text
        var senha = tfSenha.text
        
        if !isValidEmail(email) {
            var alert = UIAlertView()
            alert.title = "ERRO!"
            alert.message = "Informe um email valido!"
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        
        if  count(senha) < 6 {
            var alert = UIAlertView()
            alert.title = "ERRO!"
            alert.message = "Digite uma senha valida!"
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        
        
        loading = UIAlertView()
        loading.message = "Realizando login..."
        loading.show()
        
        makeLogin()
    }
    
    func makeLogin() {
        var manager = AFHTTPRequestOperationManager()
        
        var params = ["email":self.tfEmail.text.lowercaseString,"password":self.tfSenha.text.lowercaseString]

 
        manager.POST("http://45.55.74.184:8080/users/sign_in/", parameters: params, success: { (operation:AFHTTPRequestOperation!, responseObject: AnyObject) -> Void in
            
                self.loading.dismissWithClickedButtonIndex(0, animated: true)
            
                var token = responseObject.objectForKey("token") as! String
                var defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(token, forKey:"token")
            
                self.dismissViewControllerAnimated(true, completion: nil)
            
            }) { (operation:AFHTTPRequestOperation!, erro:NSError) -> Void in
                
                let alert = UIAlertView()
                alert.title = "Erro!"
                alert.message = "Login ou senha invalidos!"
                alert.addButtonWithTitle("OK")
                alert.show()
                
                self.loading.dismissWithClickedButtonIndex(0, animated: true)
                var defaults = NSUserDefaults.standardUserDefaults()
                defaults.removeObjectForKey("token")
                
        }
    
    
    }

}

