//
//  FeedViewController.swift
//  SwiftHeadfoneApp
//
//  Created by aluno-r17 on 09/07/15.
//  Copyright (c) 2015 TBSN. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var reviews: [Review] = []
    var loading:UIAlertView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Review"
        
        
        self.tableView.separatorColor = UIColor(red: 18.0/255.0, green: 19.0/255.0, blue: 26.0/255.0, alpha: 1.0)
        
        var login = false
        
        var defaults = NSUserDefaults.standardUserDefaults()
        
        var token:String? = defaults.valueForKey("token") as? String
        
        if token != nil {
            login = true
        }
        
        if !login {
            self.performSegueWithIdentifier("login", sender: self)
        }
        
        loading = UIAlertView()
        loading.message = "Carregando reviews..."
        loading.show()
        
        getReviews()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.reviews.count
    }
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:FeedTableViewCell = tableView.dequeueReusableCellWithIdentifier("FeedCell", forIndexPath: indexPath) as! FeedTableViewCell

        
        var review: Review = self.reviews[indexPath.row]
        
        cell.brandLabel.text = review.brand
        cell.commentLabel.text = review.comment
        cell.modelLabel.text = review.model
        
        var ratingName = "rating-\(review.rating!)-stars"
        
        cell.ratingImageView.image = UIImage(named: ratingName)
        
        
        return cell
    }
    
    func getReviews(){
        
        var manager = AFHTTPRequestOperationManager()
        
        manager.GET("http://45.55.74.184:8080/publications/list/", parameters: nil , success: { (operation:AFHTTPRequestOperation!, responseObject: AnyObject) -> Void in
            
            
                var objects:[NSDictionary] =  responseObject.objectForKey("publications") as! [NSDictionary]
            
                for object:NSDictionary in objects {
                    var review = Review()
                    review.brand = object["brand"] as? String
                    review.model = object["model"] as? String
                    review.rating = object["rating"] as? Int
                    review.comment = object["comment"] as? String
                    review.photo = object["photo"] as? String
                    self.reviews.append(review)

                }
            
                self.tableView.reloadData()
                self.loading.dismissWithClickedButtonIndex(0, animated: true)
            
            
            }) { (operation:AFHTTPRequestOperation!, erro:NSError) -> Void in
                
                
                let alert = UIAlertView()
                alert.title = "Erro!"
                alert.message = "Erro interno no servidor"
                alert.addButtonWithTitle("OK")
                alert.show()
                
                var defaults = NSUserDefaults.standardUserDefaults()
                defaults.removeObjectForKey("token")
                self.loading.dismissWithClickedButtonIndex(0, animated: true)
                
        }

    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */


}
